package ru.net.leidenn

import java.nio.ByteBuffer
import java.nio.channels.SelectionKey

class DataLink(var pair: SelectionKey? = null) {
    val input: ByteBuffer = ByteBuffer.allocate(BUF_SIZE)
    var output: ByteBuffer = ByteBuffer.allocate(BUF_SIZE)
    var inputIsReady: Boolean = false
    var currentState = States.INITIAL

    var clientSocksVersion : Byte? = null
    var clientAuthMethodCount: Byte? = null

    var addressType: Byte? = null
    var requestCommand: Byte? = null
    var domainLength: Int? = null
    var address: ByteArray? = null
    var port: Short? = null
    var reserve: Byte? = null

    enum class States {
        INITIAL,
        ESTABLISH_CONNECTION,
        IN_CONNECT,
        ERROR
    }

    companion object {
        private const val BUF_SIZE = 1024
    }


}