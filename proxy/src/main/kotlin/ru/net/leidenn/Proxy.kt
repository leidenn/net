package ru.net.leidenn

import com.sun.xml.internal.ws.api.message.Attachment
import mu.KotlinLogging
import org.xbill.DNS.*
import java.io.IOException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel

class Proxy(lhost: String, lport: Int) {
    private val log = KotlinLogging.logger("Proxy")
    private val selector = Selector.open()
    private val listener = ServerSocketChannel.open().apply {
        bind(InetSocketAddress(lhost, lport))
        configureBlocking(false)
        register(selector, SelectionKey.OP_ACCEPT)
    }
    private val resolver = SimpleResolver().apply {
        setAddress(InetAddress.getByName(ResolverConfig.getCurrentConfig().servers().first()))
    }

    fun start() {
        log.info { "start" }
        while (true) {
            if (selector.select() == 0) {
                continue
            }

            for (key in selector.selectedKeys()) {
                when {
                    key.isAcceptable -> {
                        accept()
                    }
                    key.isConnectable -> {
                        connect(key)
                    }
                    key.isReadable -> {
                        read(key)
                    }
                    key.isWritable -> {
                        write(key)
                    }
                }
            }
            selector.selectedKeys().clear()
        }
    }

    private fun accept() {
        log.info { "Accept..." }
        val client = listener.accept() ?: return
        client.configureBlocking(false)
        val clientKey = client.register(selector, SelectionKey.OP_READ)
        val clientAttachment = DataLink()
        clientKey.attach(clientAttachment)
        log.debug { "Accept client key = $clientKey" }
    }

    private fun connect(key: SelectionKey) {
        log.info { "Connecting to key $key" }
        val channel = key.channel() as SocketChannel
        if (key.attachment() == null) {
            close(key)
            return
        }
        val data = key.attachment() as DataLink
        if (data.pair == null) {
            close(key)
            return
        }
        if (channel.finishConnect()) {
            data.output = (data.pair!!.attachment() as DataLink).input
            (data.pair!!.attachment() as DataLink).output = data.input
            key.interestOps(SelectionKey.OP_READ)
            val address = ByteBuffer.allocate(Int.SIZE_BYTES)
            address.put(0)
            address.put(0)
            address.put(0)
            address.put(0)
            (data.pair?.attachment() as DataLink).sendAnswer(REQUEST_GRANTED, IPV4, address.array(), 0)
            (data.pair?.attachment() as DataLink).output.flip()
            data.pair?.interestOps(SelectionKey.OP_WRITE)
        } else {
            close(key)
        }
    }

    private fun read(key: SelectionKey) {
        log.info { "Read..." }
        val channel = key.channel() as SocketChannel
        val data = key.attachment() as DataLink
        data.input.clear()
        val readBufferCount = try {
            channel.read(data.input)
        } catch (ex: IOException) {
            close(key)
            return
        }
        if (readBufferCount > 1) {
            log.debug { "Red $readBufferCount from $key" }
            data.input.flip()
            when (data.currentState) {
                DataLink.States.INITIAL -> {
                    establishConnection(key, data)
                }
                DataLink.States.ESTABLISH_CONNECTION -> {
                    connectToServer(key, data)
                }

                DataLink.States.IN_CONNECT -> {
                    if (data.pair == null) {
                        close(key)
                        return
                    }
                    val ops = data.pair?.interestOps() ?: return
                    data.pair?.interestOps(ops or SelectionKey.OP_WRITE)
                    key.interestOps(key.interestOps() xor SelectionKey.OP_READ)
                }
                DataLink.States.ERROR -> TODO()
            }
        } else {
            close(key)
        }
    }

    private fun write(key: SelectionKey) {
        log.info { "Perform write to $key" }
        val channel = key.channel() as SocketChannel
        val data = key.attachment() as DataLink
        val writeBufferCount = try {
            channel.write(data.output)
        } catch (ex: IOException) {
            close(key)
            return
        }
        if (writeBufferCount > 0) {
            if (!data.output.hasRemaining()) {
                data.output.compact()
                key.interestOps(SelectionKey.OP_READ)
                when (data.currentState) {
                    DataLink.States.ERROR -> {
                        close(key)
                        return
                    }
                    DataLink.States.INITIAL -> {
                        data.currentState = DataLink.States.ESTABLISH_CONNECTION
                        log.debug { "Wrote $writeBufferCount for $key: first header" }
                        return
                    }
                    DataLink.States.ESTABLISH_CONNECTION -> {
                        data.currentState = DataLink.States.IN_CONNECT
                        log.debug { "Wrote $writeBufferCount for $key: second header" }
                        return
                    }
                    DataLink.States.IN_CONNECT -> {
                        log.debug { "Wrote $writeBufferCount for $key: in connect" }
                        if (data.pair == null) {
                            close(key)
                            return
                        } else {
                            data.output.clear()
                            val ops = data.pair?.interestOps() ?: throw Exception("Null pair for $key")
                            data.pair?.interestOps(ops or SelectionKey.OP_READ)
                            return
                        }
                    }
                }
            }
        } else {
            close(key)
        }
    }

    private fun establishConnection(key: SelectionKey, data: DataLink) {
        log.info { "Establish connection..." }
        when {
            data.clientSocksVersion == null && data.input.remaining() < 2 -> return
            data.clientSocksVersion == null -> {
                data.clientSocksVersion = data.input.get()
                data.clientAuthMethodCount = data.input.get()
            }
        }
        val n = data.clientAuthMethodCount?.toInt() ?: 0
        if (data.input.remaining() < n) return
        val clientAuthMethods = Array(n) {
            data.input.get()
        }
        log.debug {
            "Red from client $key first header: ${data.clientSocksVersion}|${data.clientAuthMethodCount}|${clientAuthMethods.joinToString(
                separator = ", "
            ) { it.toString() }}"
        }
        log.debug { "Make answer for client $key first header: " }
        data.output.clear()
        val answer =
            if ((data.clientSocksVersion != SOCKS_VERSION) || (!clientAuthMethods.contains(AUTHENTICATION_METHOD))) {
                data.currentState = DataLink.States.ERROR
                log.error { "Unsupported method" }
                UNSUPPORTED_METHOD
            } else {
                AUTHENTICATION_METHOD
            }
        data.sendShortAnswer(answer)
        data.output.flip()
        key.interestOps(SelectionKey.OP_WRITE)
    }

    private fun DataLink.sendAnswer(status: Byte, addressType: Byte, address: ByteArray, port: Short) {
        log.info { "Send answer $status|$addressType|$address|$port..." }
        val capacity = Byte.SIZE_BYTES * 4 +
                if (addressType == IPV4) {
                    Int.SIZE_BYTES
                } else {
                    address.size
                } + Short.SIZE_BYTES
        val answer = ByteBuffer.allocate(capacity)
        answer.put(SOCKS_VERSION)
        answer.put(status)
        answer.put(reserved)
        answer.put(addressType)
        answer.put(address)
        answer.putShort(port)
        answer.flip()
        output.put(answer)
    }

    private fun connectToServer(key: SelectionKey, data: DataLink) {
        log.info { "Connect to server..." }
        when {
            data.addressType == null && data.input.remaining() < 4 -> return
            data.addressType == null -> {
                data.clientSocksVersion = data.input.get()
                data.requestCommand = data.input.get()
                data.reserve = data.input.get()
                data.addressType = data.input.get()
            }
        }
        val addressType = data.addressType!!
        if (data.address == null)
            data.address = when (addressType) {
                IPV4 -> {
                    if (data.input.remaining() < Int.SIZE_BYTES) return
                    val buffer = ByteArray(Int.SIZE_BYTES)
                    data.input.get(buffer, 0, Int.SIZE_BYTES)
                    buffer
                }
                DOMAIN -> {
                    when {
                        data.domainLength == null && data.input.remaining() < 1 -> return
                        data.domainLength == null -> {
                            data.domainLength = data.input.get().toInt()
                        }
                    }
                    val domainLength = data.domainLength!!
                    if (data.input.remaining() < domainLength) return
                    val buffer = ByteArray(domainLength)
                    data.input.get(buffer, 0, domainLength)
                    buffer
                }
                IPV6 -> {
                    if (data.input.remaining() < 16) return
                    val buffer = ByteArray(16)
                    data.input.get(buffer, 0, 16)
                    buffer
                }
                else -> TODO()
            }

        when {
            data.port == null && data.input.remaining() < 2 -> return
            data.port == null -> {
                data.port = data.input.short
            }
        }
        log.debug { "Red from client $key second header: ${data.clientSocksVersion}|${data.requestCommand}|x|$addressType|${data.address}|${data.port}" }

        when (data.requestCommand) {
            TCP_PORT_COMMAND, UOD_COMMAND -> {
                data.sendAnswer(COMMAND_NOT_SUPPORTED, addressType, data.address!!, data.port!!)
                data.currentState = DataLink.States.ERROR
                key.interestOps(SelectionKey.OP_WRITE)
                log.error { "Red UNSUPPORTED_COMMAND from $key" }
            }
            TCP_COMMAND -> {
                if (data.clientSocksVersion != SOCKS_VERSION || data.reserve != reserved) {
                    data.sendAnswer(PROTOCOL_ERROR, addressType, data.address!!, data.port!!)
                    data.currentState = DataLink.States.ERROR
                    key.interestOps(SelectionKey.OP_WRITE)
                    log.error { "Red PROTOCOL_ERROR from $key" }
                }
                when (addressType) {
                    IPV4 -> data.pair = makeServerChannel(key, InetAddress.getByAddress(data.address), data.port!!.toInt())
                    DOMAIN -> {
                        val msg = Message()
                        msg.header.opcode = Opcode.QUERY
                        val fullDomain = String(data.address!!) + "."
                        log.debug { "Full domain $fullDomain" }
                        msg.addRecord(Record.newRecord(Name(fullDomain), Type.A, DClass.IN), Section.QUESTION)
                        resolver.sendAsync(msg, object : ResolverListener {
                            override fun handleException(id: Any?, e: java.lang.Exception?) {
                                e?.printStackTrace() ?: log.debug { "Got null exception" }
                            }

                            override fun receiveMessage(id: Any?, m: Message?) {
                                if (m == null) {
                                    log.debug { "Got null message" }
                                    return
                                }

                                log.debug { "Got message" }

                                val answer = m.getSectionArray(Section.ANSWER)
                                for (record in answer) {
                                    log.debug { record }
                                    if (record is ARecord) {
                                        log.debug { "Found address ${record.address}" }
                                        data.pair = makeServerChannel(key, record.address, data.port!!.toInt())
                                        key.interestOps(key.interestOps() xor SelectionKey.OP_READ)
                                        data.output.flip()
                                        return
                                    }
                                }
                                log.debug { "No address specified" }

                                val sect = m.getSectionArray(Section.AUTHORITY)
                                log.debug { "Authority" }
                                for (rec in sect) {
                                    log.debug { rec }
                                }

                                data.sendAnswer(HOST_UNREACHABLE, addressType, data.address!!, data.port!!)
                                data.currentState = DataLink.States.ERROR
                                key.interestOps(SelectionKey.OP_WRITE)
                            }
                        })
                        key.interestOps(0)
                        return
                    }
                    IPV6 -> {
                        data.sendAnswer(UNSUPPORTED_ADDRESS_TYPE, addressType, data.address!!, data.port!!)
                        data.currentState = DataLink.States.ERROR
                        key.interestOps(SelectionKey.OP_WRITE)
                        log.error { "Red UNSUPPORTED_ADDRESS_TYPE from $key" }
                    }
                }
            }
        }
        key.interestOps(key.interestOps() xor SelectionKey.OP_READ)
        data.output.flip()
    }

    private fun makeServerChannel(clientKey: SelectionKey, host: InetAddress, port: Int): SelectionKey {
        val server = SocketChannel.open().apply {
            configureBlocking(false)
            connect(InetSocketAddress(host, port))
        }
        val serverKey = server.register(selector, SelectionKey.OP_CONNECT)
        val serverAttachment = DataLink(clientKey)
        serverAttachment.currentState = DataLink.States.IN_CONNECT
        serverKey.attach(serverAttachment)
        return serverKey
    }

    private fun DataLink.sendShortAnswer(byte: Byte) {
        log.info { "Set Answer $byte..." }
        val answer = ByteArray(2)
        answer[0] = SOCKS_VERSION
        answer[1] = byte
        output.put(answer)
    }

    private fun close(key: SelectionKey) {
        log.info { "Close key $key..." }
        key.channel().close()
        key.cancel()
        val pair = (key.attachment() as DataLink).pair
        if (pair != null) {
            (pair.attachment() as DataLink).pair = null
        }
    }

    companion object {
        const val SOCKS_VERSION: Byte = 0x05
        const val AUTHENTICATION_METHOD: Byte = 0x00
        const val UNSUPPORTED_METHOD: Byte = 0xFF.toByte()
        const val establishTCPStreamConnection: Byte = 0x01

        const val TCP_COMMAND: Byte = 0x01
        const val TCP_PORT_COMMAND: Byte = 0x02
        const val UOD_COMMAND: Byte = 0x03

        const val IPV4: Byte = 0x01
        const val IPV6: Byte = 0x04
        const val DOMAIN: Byte = 0x03

        const val REQUEST_GRANTED: Byte = 0x00
        const val GENERAL_FAILURE: Byte = 0x01
        const val CONNECTION_NOT_ALLOWED: Byte = 0x02
        const val NETWORK_UNREACHABLE: Byte = 0x03
        const val HOST_UNREACHABLE: Byte = 0x04
        const val CONNECTION_REFUSED: Byte = 0x05
        const val TTL_EXPIRED: Byte = 0x06
        const val COMMAND_NOT_SUPPORTED: Byte = 0x07
        const val PROTOCOL_ERROR: Byte = 0x07
        const val UNSUPPORTED_ADDRESS_TYPE: Byte = 0x08


        const val reserved: Byte = 0x00
    }
}