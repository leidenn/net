package ru.nsu.net.leidenn

import java.net.InetAddress
import java.net.InetSocketAddress
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.ServerSocketChannel

class Forwarder(
    val lport: Int,
    val rhost: String,
    val rport: Int
) {
    private val forward = InetSocketAddress(InetAddress.getByName(rhost), rport)
    private val select = Selector.open()
    private val listener = ServerSocketChannel.open().apply {
        bind(InetSocketAddress(lport))
        configureBlocking(false)
        register(select, SelectionKey.OP_ACCEPT)
    }

    private var work: Boolean = true

    fun run() {
        while(work) {
            select.select()
            for(key in select.selectedKeys()) {
                when {
                    key.isAcceptable -> {

                    }

                }
            }
        }
    }

}