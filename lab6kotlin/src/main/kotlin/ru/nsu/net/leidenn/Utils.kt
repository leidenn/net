package ru.nsu.net.leidenn

import java.nio.channels.SelectionKey

fun SelectionKey.add(key: Int) {
    interestOps(interestOps() or key)
}


operator fun SelectionKey.plusAssign(key: Int) {
    interestOps(interestOps() or key)
}

operator fun SelectionKey.minusAssign(key: Int) {
    interestOps(interestOps() and key.inv())
}

fun SelectionKey.remove(key: Int) {
    interestOps(interestOps() and key.inv())
}