package ru.nsu.net.leidenn

import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.SocketChannel

class DataLink(
    val client: SocketChannel, val forward: SocketChannel, val select: Selector
) {
    val fromClient = ByteBuffer.allocate(BUFFER_SIZE)
    val fromForward = ByteBuffer.allocate(BUFFER_SIZE)

    var isEndClientRead = false
    var isEndForwardRead = false
    var isEndClientWrite = false
    var isEndForwardWrite = false

    fun read(from: SocketChannel, buffer: ByteBuffer, to: SocketChannel): Boolean {
        return if(from.read(buffer) != -1) {
//            from.keyFor(select).remove(SelectionKey.OP_READ)
//            to.keyFor(select).add(SelectionKey.OP_WRITE)
            from.keyFor(select) -= SelectionKey.OP_READ
            to.keyFor(select) += SelectionKey.OP_WRITE
            buffer.flip()
            true
        } else {
//            from.keyFor(select).remove(SelectionKey.OP_READ)
            from.keyFor(select) -= SelectionKey.OP_READ
            false
        }
    }



    fun write(from: SocketChannel, buffer: ByteBuffer, to: SocketChannel): Boolean {
        to.write(buffer)
        if(!buffer.hasRemaining()) {
            to.keyFor(select).remove(SelectionKey.OP_WRITE)
            from.keyFor(select).add(SelectionKey.OP_READ)
            buffer.clear()
            return false
        }
        return true
    }



    companion object {
        const val BUFFER_SIZE = 1024
    }
}