package ru.nsu.net.leidenn

import mu.KotlinLogging
import java.net.InetAddress
import java.net.InetSocketAddress
import java.nio.channels.*

class Forwarder(
    val lport: Int,
    rhost: String,
    rport: Int
) {
    private val log = KotlinLogging.logger("Forwarder")


    private val forward = InetSocketAddress(InetAddress.getByName(rhost), rport)
    private val select = Selector.open()
    private val listener = ServerSocketChannel.open().apply {
        bind(InetSocketAddress(lport))
        configureBlocking(false)
        register(select, SelectionKey.OP_ACCEPT)
    }
    private val connections = mutableMapOf<SocketChannel, DataLink>()


    private fun accept() {
        log.debug { "accept()" }
        log.debug { "accept client..." }
        val client = listener.accept()?.apply {
            configureBlocking(false)
            register(select, SelectionKey.OP_READ)
        } ?: return

        log.debug { "try connect..." }
        val forward = SocketChannel.open().apply {
            configureBlocking(false)
            connect(forward)
            register(select, SelectionKey.OP_CONNECT)
        }

        DataLink(client, forward, select).let {
            connections[client] = it
            connections[forward] = it
        }
        log.debug { "accept done." }
    }

    private fun connect(channel: SelectableChannel) {
        log.debug { "connect(channel=$channel)" }
        val dataLink = connections[channel] ?: kotlin.run {
            log.error { "nothing to connect" }
            return
        }
        if (dataLink.forward.finishConnect()) {
            dataLink.forward.keyFor(select).interestOps(SelectionKey.OP_READ)
            if (dataLink.fromClient.hasRemaining()) {
                dataLink.forward.keyFor(select).add(SelectionKey.OP_WRITE)
            }
        }
    }

    private fun DataLink.tryClose() {
        log.debug { "$this.tryClose()" }

        if (isEndClientRead) {
            client.shutdownInput()
        }
        if (isEndForwardRead) {
            forward.shutdownInput()
        }
        if (isEndClientWrite && isEndForwardRead) {
            client.shutdownOutput()
        }
        if (isEndForwardWrite && isEndClientRead) {
            forward.shutdownOutput()
        }

        if (isEndClientWrite &&
            isEndForwardWrite &&
            isEndClientRead &&
            isEndForwardRead
        ) {
            try {
                client.close()
            } finally {
                forward.close()
            }
            connections.remove(forward)
            connections.remove(client)
            forward.keyFor(select).cancel()
            client.keyFor(select).cancel()
            log.debug { "tryClose(). rem ${connections.size}" }
        }
    }

    private fun read(channel: SelectableChannel) {
        log.debug { "read(channel=$channel)" }
        val dataLink = connections[channel] ?: kotlin.run {
            log.error { "nothing to connect" }
            return
        }

        dataLink.run {
            when (channel) {
                forward -> {
                    log.debug { "read.forward" }
                    if (!read(forward, fromForward, client)) {
                        isEndForwardRead = true
                        tryClose()
                    }
                }
                client -> {
                    log.debug { "read.client" }
                    if (!read(client, fromClient, forward)) {
                        isEndClientRead = true
                        tryClose()
                    }
                }
            }
        }
    }

    private fun write(channel: SelectableChannel) {
        log.debug { "write(channel=$channel)" }
        val dataLink = connections[channel] ?: kotlin.run {
            log.error { "nothing to write" }
            return
        }
        dataLink.run {
            when (channel) {
                forward -> {
                    if (!write(client, fromClient, forward)) {
                        isEndForwardWrite = true
                        tryClose()
                    }
                }
                client -> {
                    if (!write(forward, fromForward, client)) {
                        isEndClientWrite = true
                        tryClose()
                    }
                }
            }
        }
    }

    private var work: Boolean = true
    fun run() {
        log.info { "start" }
        while (work) {
            select.select()
            for (key in select.selectedKeys()) {
                when {
                    key.isAcceptable -> accept()
                    key.isConnectable -> connect(key.channel())
                    key.isReadable -> read(key.channel())
                    key.isWritable -> write(key.channel())
                }
            }
            select.selectedKeys().clear()
        }
    }


}