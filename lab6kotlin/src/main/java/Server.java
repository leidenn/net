import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Server {
    private  int lport;
    private InetSocketAddress inetSocketAddress;
    private Map<SocketChannel, ConnectionData> channelMap = new HashMap<>();

    public Server(String [] args) throws UnknownHostException {
        this.lport = Integer.parseInt(args[0]);
        int rport = Integer.parseInt(args[2]);
        inetSocketAddress = new InetSocketAddress(InetAddress.getByName(args[1]), rport);

        forward();
    }
    private void forward(){
        try {
            Selector selector = Selector.open();
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(lport));
            serverSocketChannel.configureBlocking( false );
            serverSocketChannel.register( selector, SelectionKey.OP_ACCEPT );
            while(true){
                int num = selector.select();
                if (num == 0){
                    continue;
                }
                Set<SelectionKey> keys = selector.selectedKeys();
                for (SelectionKey key : keys) {
                    if (key.isValid() && key.isAcceptable()) {
                        System.out.println("accept");
                        SocketChannel socketChannel1 = serverSocketChannel.accept();
                        if(socketChannel1 == null){
                            continue;
                        }
                        socketChannel1.configureBlocking(false);
                        socketChannel1.register(selector, SelectionKey.OP_READ);


                        SocketChannel rightSocketChannel1 = SocketChannel.open();
                        rightSocketChannel1.configureBlocking(false);
                        rightSocketChannel1.connect(inetSocketAddress);
                        rightSocketChannel1.register(selector, SelectionKey.OP_CONNECT);

                        ConnectionData data = new ConnectionData(socketChannel1, rightSocketChannel1);
                        channelMap.put(socketChannel1, data);
                        channelMap.put(rightSocketChannel1, data);
                    }else if (key.isValid() && key.isConnectable()) {
                        System.out.println("connect");
                        handleConnect(key.channel(), selector);
                    } else {
                        if (key.isValid() && key.isReadable()){
                            System.out.println("read");
                            readData(key.channel(), selector);
                        }
                        if (key.isValid() && key.isWritable()) {
                            System.out.println("write");
                            writeData(key.channel(), selector);
                        }
                    }
                }
                keys.clear();
            }


        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void handleConnect(SelectableChannel channel, Selector selector) throws IOException{
        ConnectionData data = channelMap.get(channel);
        if(data.server.finishConnect()){
            data.server.keyFor(selector).interestOps(SelectionKey.OP_READ);
            if (data.serverBuffer.hasRemaining()) {
                data.server.keyFor(selector).interestOps(data.server.keyFor(selector).interestOps() | SelectionKey.OP_WRITE);
            }
        }

    }
    private void readData(SelectableChannel channel, Selector selector) throws IOException {
        ConnectionData data = channelMap.get(channel);
        int keysClient = data.client.keyFor(selector).interestOps();
        int keysServer = data.server.keyFor(selector).interestOps();
        if (data.server == channel){
            if (data.server.read(data.clientBuffer) != -1) {
                data.server.keyFor(selector).interestOps(keysServer & ~SelectionKey.OP_READ);
                data.client.keyFor(selector).interestOps(keysClient | SelectionKey.OP_WRITE);
                data.clientBuffer.flip();
                //не хочу читать из сокета сервера хочу записывать в сокета клиент //удалить флаг чтения
            }else{
                data.isEndServerRead = true;
                data.server.keyFor(selector).interestOps(keysServer & ~SelectionKey.OP_READ);
                closeData(channel, selector);
            }
        }else if (data.client == channel){
            if (data.client.read(data.serverBuffer) != -1) {
                data.client.keyFor(selector).interestOps(keysClient & ~SelectionKey.OP_READ);
                data.server.keyFor(selector).interestOps(keysServer | SelectionKey.OP_WRITE);
                //не хочу читать из клиетна хочу писать в сервер //добавить флаг записи
                data.serverBuffer.flip();
            }else {
                data.isEndClientRead = true;
                data.client.keyFor(selector).interestOps(keysClient & ~SelectionKey.OP_READ);
                closeData(channel, selector);
            }
        }

    }
    private void writeData(SelectableChannel channel, Selector selector) throws IOException {
        ConnectionData data = channelMap.get(channel);
        int keysClient = data.client.keyFor(selector).interestOps();
        int keysServer = data.server.keyFor(selector).interestOps();
        if (data.server == channel){
            data.server.write(data.serverBuffer);
            if (!data.serverBuffer.hasRemaining()) {
                data.server.keyFor(selector).interestOps(keysServer & ~SelectionKey.OP_WRITE);
                data.client.keyFor(selector).interestOps(keysClient | SelectionKey.OP_READ);
                // хочу читать из сокета клиента не хотим писать в сервер
                data.isEndServerWrite = true;
                closeData(channel, selector);
                data.serverBuffer.clear();
            }
        }else if (data.client == channel){
            data.client.write(data.clientBuffer);
            if (!data.clientBuffer.hasRemaining()) {
                data.client.keyFor(selector).interestOps(keysClient & ~SelectionKey.OP_WRITE);
                data.server.keyFor(selector).interestOps(keysServer | SelectionKey.OP_READ);
                //хотим читать из сервера не хотим писать в клиент
                data.isEndClientWrite = true;
                closeData(channel, selector);
                data.clientBuffer.clear();
            }
        }

    }

    private void closeData(SelectableChannel channel, Selector selector) throws IOException{
        ConnectionData data = channelMap.get(channel);
        if(data.isEndClientRead){
            data.client.shutdownInput();
        }
        if(data.isEndServerRead){
            data.server.shutdownInput();
        }
        if(data.isEndClientWrite && data.isEndServerRead){
            data.server.shutdownOutput();
        }
        if(data.isEndServerWrite && data.isEndClientRead){
            data.client.shutdownOutput();
        }
        if (data.isEndClientWrite &&
                data.isEndServerWrite &&
                data.isEndClientRead &&
                data.isEndServerRead){
            data.client.close();
            data.server.close();
            channelMap.remove(data.server);
            channelMap.remove(data.client);
            data.server.keyFor(selector).cancel();
            data.client.keyFor(selector).cancel();
            System.out.println("close");
        }
    }

    private static class ConnectionData{
        ByteBuffer clientBuffer = ByteBuffer.allocate(2048);
        ByteBuffer serverBuffer = ByteBuffer.allocate(2048);
        SocketChannel client;
        SocketChannel server;
        private boolean isEndClientRead = false;
        private boolean isEndServerRead = false;
        private boolean isEndClientWrite = false;
        private boolean isEndServerWrite = false;
        ConnectionData(SocketChannel client, SocketChannel server){
            this.client = client;
            this.server = server;
        }

    }

}
