import java.io.*
import java.net.Socket

class Client(ip: String, port: Int, filename: String) {
    val file = File(filename)
    val socket = Socket(ip, port)
    val output = DataOutputStream(socket.getOutputStream())
    val input = DataInputStream(socket.getInputStream())


    init {
        file.validate()


        val reader = FileInputStream(file) // FIXME close!! done

        try {
            println("send file name length...")
            output.writeInt(file.name.length)

            println("send file name...")
            output.write(file.name.toByteArray())

            println("send file length...")
            output.writeLong(file.length())

            val msg = ByteArray(MSG_SIZE)
            println("read from file...")
            var msgSize = reader.read(msg, 0, MSG_SIZE)
            while (msgSize > 0) {
                println("send data pack length...")
                output.write(msg, 0, msgSize)
                println("read from file...")
                msgSize = reader.read(msg, 0, MSG_SIZE)
            }
            println("end send data")

            input.read(msg, 0, 1)
            if (msg.first() != 1.toByte()) println("Server error")
            else println("Done.")
        } catch (ex: IOException) {
            println("Error while sending")
        }
        reader.close()
        socket.close()
    }
}
