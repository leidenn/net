import sun.font.CreatedFontTracker.MAX_FILE_SIZE
import java.io.File

const val MAX_FILENAME_LENGTH = 20
const val MSG_SIZE = 32
const val TIMEOUT = 5000

fun File.validate() {
    if (!exists()) throw Error("No such file")
    if (length() > MAX_FILE_SIZE) throw Error("Too big file")
    if (name.length > MAX_FILENAME_LENGTH) throw Error("Too long filename")
}
