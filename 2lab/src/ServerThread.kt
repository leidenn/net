import sun.font.CreatedFontTracker.MAX_FILE_SIZE
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.Socket
import java.net.SocketTimeoutException
import java.nio.charset.Charset

class ServerThread(val client: Socket) : Thread() {

    val input = DataInputStream(client.getInputStream())
    val output = DataOutputStream(client.getOutputStream())
    var fileName = ""

    var recvSize: Long = 0L
        @Synchronized set
        @Synchronized get

    override fun run() {

        val msg = ByteArray(32)

        val nameLength = input.readInt()
        if (nameLength > MAX_FILENAME_LENGTH) {
            client.close()
            return
        }

        //val recvNameLength: Int
        if (input.read(msg, 0, nameLength) != nameLength) {
            println("Cant read filename")
            client.close()
            return
        }

        fileName = String(msg.sliceArray(0 until nameLength), Charset.forName("UTF-8"))
        val fileLength = input.readLong()
        if (fileLength > MAX_FILE_SIZE) {
            client.close()
            return
        }

        println("$fileName: File size - ${fileLength.toDouble() / 1024} KB")


        // TODO check name
        val file = File("./uploads/${File(fileName).name}")
        if (file.exists()) {
            file.delete()
        }

        println("create file $fileName for $client")
        file.createNewFile()
        val fileOut = FileOutputStream(file)
        client.soTimeout = TIMEOUT
        val timer = Timer(this).also { it.start() }
        try {
            println("start recv data from $client")
            while (recvSize < fileLength) {
                val received = input.read(msg, 0, MSG_SIZE)
                fileOut.write(msg, 0, received)
                recvSize += received
            }
            timer.interrupt()
        } catch (ex: SocketTimeoutException) {
            println("timeout in $client ($recvSize/$fileLength)")
            timer.interrupt()
            fileOut.close()
            file.delete()
            client.close()
            return
        }
        fileOut.close()
        file.delete()
        output.write(ByteArray(1) { 1 })
        client.close()
        println("$fileName: Downloading done")
    }


}
