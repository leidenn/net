import java.io.File
import java.net.ServerSocket

class Server(private val port: Int) {

    val server = ServerSocket(port)

    init {
        val dir = File("./uploads")
        if (!dir.exists()) {
            dir.mkdir()
        }
    }

    fun update() {
        try {
            val client = server.accept()
            println("accept $client")
            ServerThread(client).also {
                it.start()
            }
        } catch (e: Exception) {
            error("cant accept connection $e")
        }
    }

    fun end() {
        server.close()
    }
}



