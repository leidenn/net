class Timer(private val serverThread: ServerThread) : Thread() {
    private val start = System.currentTimeMillis()
    private var size = 0L


    override fun run() {
        while (!isInterrupted) {
            try {
                Thread.sleep(1000)
            } catch (ex: InterruptedException) {
                break
            }
            showMomentSpeed()
            showAverageSpeed()
            size = serverThread.recvSize
        }
        showAverageSpeed()
    }

    private fun showMomentSpeed() {
        val received = serverThread.recvSize - size
        val speed = received.toDouble() / 1024
        println("${serverThread.fileName}: Current speed - $speed KB/s")
    }

    private fun showAverageSpeed() {
        size = serverThread.recvSize
        val curTime = System.currentTimeMillis()
        val fullTime = (curTime.toDouble() - start.toDouble()) / 1000.0
        val avSpeed = (size.toDouble() / fullTime) / 1024.0
        println("${serverThread.fileName}: Average speed - $avSpeed KB/s")
    }
}
