package ru.nsu.leidenn

import java.time.LocalDateTime

data class Message(
    val username: String? = null,
    val message: String? = null,
    val time: LocalDateTime? = null,
    val token: String? = null,
    val author: Int? = null
)

data class User(
    val id: Int? = null,
    val username: String? = null,
    val online: Boolean? = null
)

data class UsersList(
    val users: List<User>?
)

data class MessagesList(
    val messages: List<Message>?
)