package ru.nsu.leidenn

import com.google.gson.Gson
import okhttp3.*
import java.time.LocalTime
import java.util.*

class Client(host: String, port: Int) {
    private val client = OkHttpClient()
    private val url = "http://$host:$port"
    private val gson = Gson()
    private var token: UUID? = null
    private var work = true
    private val messages = mutableListOf<Message>()
    private var users = listOf<User>()


    init {
        login()
        IO(this).also { it.start() }
        while (work) {
            Thread.sleep(1000)
            users()
            messages()
        }
        println("end")
    }

    fun messages(offset: Int = 0, count: Int = 10) {
        val request = Request.Builder()
            .url("$url/messages/offset=$offset&count=$count")
            .header("Authorization", "Token ${token.toString()}")
            .get()
            .build()
        val response = client.newCall(request).execute()
        val msgs =
            gson.fromJson(response.body()?.string() ?: error("messages"), MessagesList::class.java).messages ?: return

        val new = msgs.minus(messages).sortedBy { it.time }
        messages.addAll(new)
        for (msg in new) {
            println("${users.find { it.id == msg.author }?.username}|${msg.time?.toLocalTime()}: ${msg.message}")
        }
    }

    fun users(id: Int? = null) {
        val userUrl = "$url/users" + if (id != null) "/$id" else ""
        val request = Request.Builder()
            .url(userUrl)
            .header("Authorization", "Token ${token.toString()}")
            .get()
            .build()
        val response = client.newCall(request).execute()
        val u = gson.fromJson(response.body()?.string() ?: error("users"), UsersList::class.java).users ?: return
        synchronized(users) {
            users = u
        }
    }

    private fun login() {
        var login = false
        while (!login) {
            print("Enter username: ")
            val username = readLine()?.trim() ?: continue
            val body = RequestBody.create(JSON, gson.toJson(Message(username = username)))
            val request = Request.Builder()
                .url("$url/login")
                .post(body)
                .build()
            println("send login request...")
            val response = client.newCall(request).execute()
            if (response.code() == 409) {
                println("409")
            } else {
                token = UUID.fromString(gson.fromJson(response.body()?.charStream(), Message::class.java).token)
                println("You log in.")
                login = true
            }
        }
    }

    fun sendMessage(str: String) {
        val body = RequestBody.create(JSON, gson.toJson(Message(message = str)))
        val request = Request.Builder()
            .url("$url/messages")
            .header("Authorization", "Token ${token.toString()}")
            .post(body)
            .build()
        client.newCall(request).execute()
    }

    fun logout() {
        val request = Request.Builder()
            .url("$url/logout")
            .get()
            .build()
        client.newCall(request).execute()
        synchronized(work) {
            work = false
        }
    }

    private companion object {
        val JSON = MediaType.parse("application/json")
    }
}