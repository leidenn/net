package ru.nsu.leidenn

import java.io.InterruptedIOException

class IO(val client: Client) : Thread() {
    override fun run() {
        while (!isInterrupted) {
            try {
                val str = readLine() ?: continue
                if (str.trim().firstOrNull() == '/') {
                    cmd(str)
                } else {
                    client.sendMessage(str)
                }
            } catch (e: InterruptedIOException) {
                println("by")
                break
            }
        }
    }

    private fun cmd(str: String) {
        when (str.trim()) {
            "/exit" -> {
                client.logout()
                interrupt()
            }
            "/users" -> client.users()
        }
    }
}