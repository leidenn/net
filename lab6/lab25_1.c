#include	<sys/types.h>	/* basic system data types */
#include	<sys/socket.h>	/* basic socket definitions */
#include	<sys/time.h>	/* timeval{} for select() */
#include	<time.h>		/* timespec{} for pselect() */
#include	<netinet/in.h>	/* sockaddr_in{} and other Internet defns */
#include	<fcntl.h>		/* for nonblocking */
#include	<netdb.h>
#include	<signal.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<sys/uio.h>		/* for iovec{} and readv/writev */
#include	<unistd.h>
#include    <assert.h>

/*
 * 
 */

#define BUFSIZE 4096
#define DEBUG

char * usage_format="Usage: %s listen_port remote_host remote_port\n";

struct connection_descriptor {
    int client_socket, forwarding_socket;
    struct connection_descriptor *prev, *next;
    ssize_t data_c_f, data_f_c; /* data counters.  -1 designates closed target connection */
    char buf_c_f[BUFSIZE]; /* data from client to forward */
    char buf_f_c[BUFSIZE]; /* data from forward to client */
};

struct connection_descriptor *head, *tail;

int nfds = 0, cur_connections = 0;
int max_descs;

int listenfd;

void release(int sig){
	if (close(listenfd)){			// not working
		perror("Closing socket:");
	} else {
		printf("\nSocket closed successfully.\n");
	}
	exit(0);
}

void check_socket_against_nfds(int s) {
    if (s>=FD_SETSIZE) {
        fprintf(stderr, "Socket number out of range for select\n");
        exit(0);
    }
    if (s+1>nfds) nfds=s+1;
}

struct connection_descriptor *add_connection(int client_socket, struct sockaddr *servaddr) {
    if (cur_connections + 1 >= max_descs){
        printf("Number of connections is ahead of limit\n");
        close(client_socket);
        cur_connections--;
        return NULL;
    }

    struct connection_descriptor *t;
    t=malloc(sizeof(struct connection_descriptor));
    t->client_socket=client_socket;
    t->forwarding_socket=socket(AF_INET, SOCK_STREAM, 0);
    check_socket_against_nfds(t->forwarding_socket);
    if (connect(t->forwarding_socket, servaddr, sizeof(*servaddr))) {
        perror("Connecting to server:");
        close(t->client_socket);
        cur_connections--;
        free(t);
        return NULL;
    }
    cur_connections++;
    t->prev=NULL;
    t->next=head;
    if (head==NULL) {
        tail=t;
    } else {
        head->prev=t;
    }
    head=t;
    t->data_f_c=t->data_c_f=0;
    memset(t->buf_c_f, 0, sizeof(t->buf_c_f));
    memset(t->buf_f_c, 0, sizeof(t->buf_f_c));

    #ifdef DEBUG
    	printf("Added forward connection, connection number: %d\n", cur_connections);
    #endif

    return t;
}

void drop_connection(struct connection_descriptor *t) {
    if (t==head && t==tail) { 
        /* it was only record in the list */
        head=tail=NULL;
    } else if (t==head) {
        head=t->next;
        head->prev=NULL;
    } else if (t==tail) {
        tail=t->prev;
        tail->next=NULL;
    } else {
        assert(t->next!=NULL);
        assert(t->prev!=NULL);
        t->prev->next=t->next;
        t->next->prev=t->prev;
    }
    close(t->client_socket);
    close(t->forwarding_socket);
    free(t);

    cur_connections -= 2;

    #ifdef DEBUG
    	printf("Dropped client and foward connections, connections number: %d\n", cur_connections);
    #endif
}

void form_select_masks(fd_set *readfs, fd_set *writefs, int listenfd) {
    struct connection_descriptor *t;
    
    FD_ZERO(readfs); FD_ZERO(writefs);
    FD_SET(listenfd, readfs);
    t=head;
    while(t) {
        struct connection_descriptor *t1;
        t1=t->next;
        if((t->data_c_f<0 && t->data_f_c<=0) || (t->data_f_c<0 && t->data_c_f<=0)) {
            drop_connection(t);
        } else {
            if(t->data_c_f==0) 
                FD_SET(t->client_socket, readfs);
            if(t->data_f_c==0) 
                FD_SET(t->forwarding_socket, readfs);
            if(t->data_c_f>0) 
                FD_SET(t->forwarding_socket, writefs);
            if(t->data_f_c>0) 
                FD_SET(t->client_socket, writefs);
        }
        t=t1;
    }
}

void test_and_process_masks(fd_set *readfs, fd_set *writefs) {
    /* listendf socket is tested outside of this and after this */
    struct connection_descriptor *t;
    
    t=head;
    while(t) {
        if(t->data_c_f==0 && FD_ISSET(t->client_socket, readfs)) {
            t->data_c_f=read(t->client_socket, t->buf_c_f, sizeof(t->buf_c_f));
            if(t->data_c_f==0) t->data_c_f=-1;
        }
        if(t->data_f_c==0 && FD_ISSET(t->forwarding_socket, readfs)) {
            t->data_f_c=read(t->forwarding_socket, t->buf_f_c, sizeof(t->buf_f_c));
            if(t->data_f_c==0) t->data_f_c=-1;
        }
        if(t->data_c_f>0 && FD_ISSET(t->forwarding_socket, writefs)) {
            int res=write(t->forwarding_socket, t->buf_c_f, t->data_c_f);
            if (res==-1) t->data_f_c=-1;
            else t->data_c_f=0;  /* assume that write always sucessful with full size */
        }
        if(t->data_f_c>0 && FD_ISSET(t->client_socket, writefs)) {
            int res=write(t->client_socket, t->buf_f_c, t->data_f_c);
            if (res==-1) t->data_c_f=-1;
            else t->data_f_c=0;
        }
            
        t=t->next;
    }
}    
        
int handle_host_status(int status, char** argv){
    if (status != 0){
        char *errors[]={ 
            "Address family not supported", 
            "Socket type not supported", 
            "Bad flags", 
            "Host not found", 
            "Service not avaliable for current socket type", 
            "No address in requested family",
            "Host have no address",
            "No memory",
            "Server is not avaliable",
            "Server is not avaliable now, try again later",
            "System error",
            "Unknown error" 
        };
        char *err;
        switch (status) {
            case EAI_FAMILY:
                err = errors[0];
                break;
            case EAI_SOCKTYPE:
                err = errors[1];
                break;
            case EAI_BADFLAGS:
                err = errors[2];
                break;    
            case EAI_NONAME:
                err = errors[3];
                break;
            case EAI_SERVICE:
                err = errors[4];
                break;
            case EAI_ADDRFAMILY:
                err = errors[5];
                break;
            case EAI_NODATA:
                err = errors[6];
                break;
            case EAI_MEMORY:
                err = errors[7];
                break;
            case EAI_FAIL:
                err = errors[8];
                break;
            case EAI_AGAIN:
                err = errors[9];
                break;
            case EAI_SYSTEM:
                err = errors[10];
                break;
            default:
                err = errors[11];
                break;
        }
        
        fprintf(stderr, "%s: %s:%s %s\n", argv[0], argv[2], argv[3], err);
    }
    return status;
}

int main(int argc, char** argv) {

    if (argc<3) {
        fprintf(stderr, usage_format, argv[0]);
        return 0;
    }
    
    int listen_port = atoi(argv[1]);
    int remote_port = atoi(argv[3]);

    if (listen_port <= 0 || remote_port <= 0) {
        fprintf(stderr, usage_format, argv[0]);
        return 0;
    }

    struct rlimit rlim;
    getrlimit(RLIMIT_NOFILE, &rlim);
    max_descs = rlim.rlim_cur - 4;

    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = PF_INET;
    struct addrinfo *addr_info;
    int status = getaddrinfo(argv[2], argv[3], &hints, &addr_info);
    if (handle_host_status(status, argv)) {
        return 0;
    }

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    check_socket_against_nfds(listenfd);

    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(listen_port);

    if (bind(listenfd, (struct sockaddr*) &servaddr, sizeof(servaddr))) {
        perror(argv[0]);
        return 0;
    }

    signal(SIGPIPE, SIG_IGN);
    signal(SIGINT, release);
    listen(listenfd, SOMAXCONN);

    #ifdef DEBUG
        printf("Max connections: %d\n", max_descs - 1);
    #endif

    fd_set readfs, writefs;
    while(1) {
        form_select_masks(&readfs, &writefs, listenfd);
        int nready = select(nfds, &readfs, &writefs, NULL, NULL);
        if (nready < 0){
            perror(argv[0]);
            continue;
        } else if (nready == 0) {
            continue;
        } else {
            test_and_process_masks(&readfs, &writefs);
            if (FD_ISSET(listenfd, &readfs)) {
                struct sockaddr_in cliaddr;
                int addrlen = sizeof(cliaddr);
                if (cur_connections + 1 >= max_descs){
                    printf("Number of connections is ahead of limit\n");
                    continue;
                }
                int client_connection = accept(listenfd, (struct sockaddr*)&cliaddr, &addrlen);
                if (client_connection < 0){
                    perror(argv[0]);
                    continue;
                } else {
                    check_socket_against_nfds(client_connection);
                    cur_connections++;
                    #ifdef  DEBUG
                        printf("Added client connection, connection number: %d\n", cur_connections);
                    #endif
                    struct sockaddr *forwaddr = addr_info->ai_addr;
                    add_connection(client_connection, forwaddr);
                }
            }
        }
    }
    return 0;
}


