#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>

// For getrlimit
#include <sys/time.h>
#include <sys/resource.h>
// For addrinfo
#include <netdb.h>
// For memset
#include <string.h>
#include <zconf.h>
#include <assert.h>
#include <signal.h>


int maxDescs;
int listenFd;
int nfds = 0;


typedef struct Connection {
    int clientSocket;
    int forwardSocket;
    ssize_t fromClient;
    ssize_t fromForward;
    char clientBuffer[BUFSIZ];
    char forwardBuffer[BUFSIZ];
} Connection;

typedef struct ConnectionNode {
    Connection connection;
    struct ConnectionNode *prev;
    struct ConnectionNode *next;
} ConnectionNode;


ConnectionNode *head, *tail;
ssize_t size = 0;

int checkNfds(int s) {
    if (s >= FD_SETSIZE) {
        fprintf(stderr, "nfds error: %d > %d\n", s, nfds);
        return -1;
    }
    if (s + 1 > nfds) nfds = s + 1;
    return nfds;
}

ConnectionNode *addConnection(int clientSocket, struct sockaddr *servaddr) {
    if (size + 1 >= maxDescs) {
        printf("Number of connections is ahead of limit\n");
        close(clientSocket);
        size--;
        return NULL;
    }

    ConnectionNode *node;
    node = (ConnectionNode *) malloc(sizeof(ConnectionNode));
    node->connection.clientSocket = clientSocket;
    node->connection.forwardSocket = socket(AF_INET, SOCK_STREAM, 0); // TODO make forw nonblock
    checkNfds(node->connection.forwardSocket);
    if (connect(node->connection.forwardSocket, servaddr, sizeof(*servaddr))) {
        perror("Connecting to server:");
        close(node->connection.clientSocket);
        size--;
        free(node);
        return NULL;
    }
    size++;
    node->prev = NULL;
    node->next = head;
    if (head == NULL) {
        tail = node;
    } else {
        head->prev = node;
    }
    head = node;
    node->connection.fromForward = node->connection.fromClient = 0;
    memset(node->connection.clientBuffer, 0, sizeof(node->connection.clientBuffer));
    memset(node->connection.forwardBuffer, 0, sizeof(node->connection.forwardBuffer));
    return node;
}

void removeConnection(ConnectionNode *connection) {
    fprintf(stderr, "Remove connection...\n");
    if (connection == head && connection == tail) {
        head = tail = NULL;
    } else if (connection == head) {
        head = connection->next;
        head->prev = NULL;
    } else if (connection == tail) {
        tail = connection->prev;
        tail->next = NULL;
    } else {
        assert(connection->next != NULL);
        assert(connection->prev != NULL);
        connection->prev->next = connection->next;
        connection->next->prev = connection->prev;
    }
    close(connection->connection.clientSocket);
    close(connection->connection.forwardSocket);
    free(connection);

    size -= 2;
    fprintf(stderr, "Removed\n");
}

void updateFd(fd_set *readFds, fd_set *writeFds, int listenfd) {
    FD_ZERO(readFds);
    FD_ZERO(writeFds);
    FD_SET(listenfd, readFds);
    ConnectionNode *connection = head;
    while (connection) {
        ConnectionNode *next = connection->next;
        // TODO wrong
        if ((connection->connection.fromClient < 0 && connection->connection.fromForward <= 0)
            || (connection->connection.fromForward < 0 && connection->connection.fromClient <= 0)) {
            removeConnection(connection);
        } else {
            if (connection->connection.fromClient == 0)
                FD_SET(connection->connection.clientSocket, readFds);
            if (connection->connection.fromForward == 0)
                FD_SET(connection->connection.forwardSocket, readFds);
            if (connection->connection.fromClient > 0)
                FD_SET(connection->connection.forwardSocket, writeFds);
            if (connection->connection.fromForward > 0)
                FD_SET(connection->connection.clientSocket, writeFds);
        }
        connection = next;
    }
}

void doReadWrite(fd_set *readFds, fd_set *writeFds) {
    ConnectionNode *node;
    node = head;
    while (node) {
        if (node->connection.fromClient == 0 && FD_ISSET(node->connection.clientSocket, readFds)) {
            node->connection.fromClient = read(
                    node->connection.clientSocket,
                    node->connection.clientBuffer,
                    sizeof(node->connection.clientBuffer)
            );
            if (node->connection.fromClient == 0) node->connection.fromClient = -1;
        }
        if (node->connection.fromForward == 0 && FD_ISSET(node->connection.forwardSocket, readFds)) {
            node->connection.fromForward = read(
                    node->connection.forwardSocket,
                    node->connection.forwardBuffer,
                    sizeof(node->connection.forwardBuffer)
            );
            if (node->connection.fromForward == 0) node->connection.fromForward = -1;
        }
        if (node->connection.fromClient > 0 && FD_ISSET(node->connection.forwardSocket, writeFds)) {
            ssize_t res = write(
                    node->connection.forwardSocket,
                    node->connection.clientBuffer,
                    (size_t) node->connection.fromClient
            );
            if (res == -1) node->connection.fromForward = -1;
            else node->connection.fromClient = 0; // TODO from - res
        }
        if (node->connection.fromForward > 0 && FD_ISSET(node->connection.clientSocket, writeFds)) {
            ssize_t res = write(
                    node->connection.clientSocket,
                    node->connection.forwardBuffer,
                    (size_t) node->connection.fromForward
            );
            if (res == -1) node->connection.fromClient = -1;
            else node->connection.fromForward = 0;
        }
        node = node->next;
    }
}

struct addrinfo *nameResolve(char *host, char *port) {
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    struct addrinfo *addrInfo;
    int res = getaddrinfo(host, port, &hints, &addrInfo);
    if (res < 0) {
        return NULL;
    }
    return addrInfo;
}

int newSocket() {
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        return -1;
    }
    checkNfds(s);
    return s;
}

int bindSocket(int fd, int port) {
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(port);
    if (bind(fd, (struct sockaddr *) &addr, sizeof(addr))) {
        return -1;
    } else {
        return 0;
    }
}

void newConnection(struct addrinfo *addrInfo) {
    struct sockaddr_in clientAddr;
    int addrLen = sizeof(clientAddr);
    if (size + 1 > maxDescs) {
        printf("have max connections\n");
        return;
    }
    int clientConnection = accept(listenFd, (struct sockaddr *) &clientAddr, &addrLen);
    if (clientConnection < 0) {
        perror("accept");
        return;
    } else {
        checkNfds(clientConnection);
        size++;
        struct sockaddr *forwardAddr = addrInfo->ai_addr;
        addConnection(clientConnection, forwardAddr);
    }
}

void processFds(fd_set *readFds, fd_set *writeFds, struct addrinfo *addrInfo) {
    int n = select(nfds, readFds, writeFds, NULL, NULL); // TODO check sock errors
    if (n < 0) {
        return;
    } else if (n == 0) {
        return;
    } else {
        doReadWrite(readFds, writeFds);
        if (FD_ISSET(listenFd, readFds)) {
            newConnection(addrInfo);
        }
    }
}

void release(int sig) {
    fprintf(stderr, "close listen...\n");
    if (close(listenFd)) {            // not working
        perror("Closing socket:");
    } else {
        fprintf(stderr, "Socket closed successfully.\n");
    }
    ConnectionNode *t = head;
    fprintf(stderr, "Remove connections...\n");
    while (t) {
        ConnectionNode *t1 = t->next;
        removeConnection(t);
        t = t1;
    }
    fprintf(stderr, "Done.\n");
    exit(0);
}

int main(int argc, char **argv) {
    signal(SIGINT, release);
    if (argc < 3) {
        fprintf(stderr, "Usage: %s listen_port remote_host remote_port\n", argv[0]);
        return -1;
    }

    int lport = atoi(argv[1]);
    char *rhost = argv[2];
    int rport = atoi(argv[3]);

    if (lport <= 0 || rport <= 0) {
        fprintf(stderr, "Port < 0\n");
        return -1;
    }

    struct rlimit rlim;
    getrlimit(RLIMIT_NOFILE, &rlim);
    maxDescs = (int) (rlim.rlim_cur - 4);

    struct addrinfo *addrInfo = nameResolve(rhost, argv[3]);
    if (addrInfo == NULL) {
        perror("nameResolve");
        return -1;
    }


    listenFd = newSocket();
    if (listenFd == -1) {
        perror("newSocket");
        return -1;
    }

    if (bindSocket(listenFd, lport) == -1) {
        perror("bindSocket");
        close(listenFd);
        return -1;
    }

    int res = listen(listenFd, SOMAXCONN);
    if (res == -1) {
        perror("listen");
        close(listenFd);
        return -1;
    }

    fd_set readFds, writeFds;
    while (1) {
        updateFd(&readFds, &writeFds, listenFd);
        processFds(&readFds, &writeFds, addrInfo);
    }
}