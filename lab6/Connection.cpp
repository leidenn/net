//
// Created by leidenn on 12/11/18.
//

#include <cstring>
#include "Connection.h"

int Connection::nfds = 0;

Connection::Connection(int client, struct sockaddr *host) {
    clientSocket = client;
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket >= FD_SETSIZE)
        throw std::range_error("Socket number out of range for select");
    if (serverSocket + 1 > Connection::nfds) Connection::nfds++;
//    if (connect(serverSocket, (struct sockaddr *) &host->sin_family, sizeof(*host))) {
    if (connect(serverSocket, host, sizeof(*host))) {
        close(clientSocket);
        std::perror("Connecting to server");
        throw std::runtime_error("Connecting to server");
    }
    fromClient = fromServer = 0;
    memset(clientBuffer, 0, sizeof(clientBuffer));
    memset(serverBuffer, 0, sizeof(serverBuffer));
}

Connection::~Connection() {
    close(clientSocket);
    close(serverSocket);
}
