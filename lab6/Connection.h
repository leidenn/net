//
// Created by leidenn on 12/11/18.
//

#ifndef LAB6_CONNECTION_H
#define LAB6_CONNECTION_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <unistd.h>
#include <cstdio>

#define BUFSIZE 4096

class Connection {
public:
    Connection(int client, struct sockaddr *host);
    ~Connection();

    static int nfds;
    int clientSocket;
    int serverSocket;
    ssize_t fromClient;
    ssize_t fromServer;
    char clientBuffer[BUFSIZE];
    char serverBuffer[BUFSIZE];
};

#endif //LAB6_CONNECTION_H
