package ru.nsu.leidenn

import ru.nsu.leidenn.client.Client
import ru.nsu.leidenn.server.Server

fun main(args: Array<String>) {
    val host = "localhost"
    val port = 8080
    when (args.firstOrNull()?.toLowerCase()) {
        "s" -> Server(host, port)
        "c" -> Client(host, port)
    }
}