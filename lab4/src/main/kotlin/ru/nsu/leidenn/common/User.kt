package ru.nsu.leidenn.common

import java.time.LocalDateTime
import java.util.*

data class User(
    val id: Int? = null,
    val username: String? = null,
    val token: UUID? = null,
    val online: Boolean? = null,
    var lastActive: LocalDateTime? = null
)

data class UsersList(
    val users: List<User>
)