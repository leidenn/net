package ru.nsu.leidenn.common

import com.google.gson.Gson
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*


data class Message(
    val type: MessageType,
    val username: String? = null,
    val id: Int? = null,
    val message: String? = null,
    val time: LocalDateTime? = null,
    val author: Int? = null,
    val token: String? = null
) {
    enum class MessageType {
        TEXT,
        LOGOUT,
        LOGIN,
        ERROR
    }
    companion object {
        const val NOT_AUTH = "You are not authorized."
    }
}

data class MessagesList(
    val messages: List<Message>
)
//
//fun HttpServerExchange.send(message: Message, gson: Gson = Gson()) {
//    responseHeaders.clear()
//    responseHeaders.put(Headers.CONTENT_TYPE, "application/json")
//    responseSender.send(gson.toJson(message))
//}
//
//fun HttpServerExchange.error(code: Int, str: String, gson: Gson = Gson()) {
//    statusCode = code
//    send(
//        Message(
//            null,
//            str,
//            null
//        ), gson
//    )
//}