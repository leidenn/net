package ru.nsu.leidenn

import java.io.InterruptedIOException



class IO(val client: Server) : Thread() {
    override fun run() {
        while (!isInterrupted) {
            try {
                println("readline")
                val str = readLine() ?: continue
                if (str.trim().firstOrNull() == '/') {
                    cmd(str)
                } else {
                    // TODO
                }
            } catch (e: InterruptedIOException) {
                println("by")
                break
            }
        }
    }

    private fun cmd(str: String) {
        when (str.trim()) {
            "/exit" -> {
                client.exit()
                interrupt()
            }
            "/clear_users" -> {
                client.clearUsers()
            }
        }
    }
}