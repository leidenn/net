package ru.nsu.leidenn

import com.google.gson.Gson
import io.undertow.Handlers
import io.undertow.Undertow
import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import java.time.LocalDateTime
import java.util.*

data class User(val id: Int, val username: String, val online: Boolean, val token: UUID, var lastActive: LocalDateTime)

class Server(hostName: String, port: Int) {
    private val gson = Gson()
    private val users: MutableList<User> = mutableListOf()
    private val messages: MutableList<Message.SimpleMessage> = mutableListOf()
    private var work = true

    fun exit() {
        synchronized(work) {
            work = false
        }
    }

    fun clearUsers() {
        synchronized(users) {
            users.clear()
        }
    }

    private fun login(exchange: HttpServerExchange) {
        println("<><><><><><><><><><> login <><><><><><><><><><>")
        if (!exchange.isPost) {
            exchange.error(405, "Wrong http request method. Must be POST", gson)
            return
        }
        exchange.requestReceiver.receiveFullString { _: HttpServerExchange?, message: String? ->
            val j = gson.fromJson(message, Message.InitMessage::class.java)
            println("try login with username: ${j.username}")
            if (j?.username == null) {
                exchange.error(400, "Request has wrong body.", gson)
                return@receiveFullString
            }
            if (users.find { it.username == j.username } != null) {
                println("This username already auth")
                exchange.error(409, "This username already auth.", gson)
                return@receiveFullString
            }
            val user = User(
                ((users.maxBy { it.id }?.id ?: 0) + 1) % Int.MAX_VALUE,
                j.username.toString(),
                true,
                UUID.randomUUID(),
                LocalDateTime.now()
            )
            println("login as: $user")
            synchronized(users){
                users.add(user)
            }
            exchange.send(
                Message.AboutUser(
                    user.id,
                    user.username,
                    user.online,
                    user.token.toString()
                ),
                gson
            )
        }
    }

    private fun logout(exchange: HttpServerExchange) {
        println("<><><><><><><><><><> logout <><><><><><><><><><>")
        if (!exchange.isPost) {
            exchange.error(405, "Wrong http request method. Must be POST", gson)
            return
        }
        if (!isAuthorized(exchange)) {
            exchange.error(403, Message.NOT_AUTH, gson)
            return
        }
        val token = exchange.token
        synchronized(users){
            users.removeIf { user ->
                user.token == token
            }
        }
        exchange.send(Message.SimpleMessage(null, "bye!", null))
    }

    private fun users(exchange: HttpServerExchange) {
        println("<><><><><><><><><><> users <><><><><><><><><><>")
        if (!exchange.isGet) {
            exchange.error(405, "Wrong http request method. Must be GET", gson)
            return
        }
        if (!isAuthorized(exchange)) {
            exchange.error(403, Message.NOT_AUTH, gson)
            return
        }
        if (exchange.relativePath.isEmpty()) {
            exchange.send(Message.UsersList(users.mapIndexed { index, user ->
                Message.AboutUser(
                    user.id,
                    user.username,
                    true
                )
            }), gson)
        } else {
            val index = exchange.relativePath.substring(1).toInt()
            if (users.elementAtOrNull(index) == null) {
                exchange.statusCode = 404
                exchange.send(Message.SimpleMessage(null, "There is no user with this id"), gson)
            } else {
                exchange.send(
                    Message.AboutUser(
                        index,
                        users[index].username,
                        true
                    )
                )
            }
        }

    }

    private fun messages(exchange: HttpServerExchange) {
        println("<><><><><><><><><><> messages <><><><><><><><><><>")
        if (!isAuthorized(exchange)) {
            exchange.error(403, Message.NOT_AUTH, gson)
            return
        }

        // FIXME It looks confusing
        when {
            exchange.isGet -> sendMessages(exchange)
            exchange.isPost -> getMessage(exchange)
        }
    }

    private fun sendMessages(exchange: HttpServerExchange) {
        val offset = exchange.queryParameters["offset"]?.first?.toInt() ?: 0
        val count = exchange.queryParameters["count"]?.first?.toInt() ?: 10
        val last = if (offset + count > messages.size)
            messages.size
        else
            offset + count
        exchange.send(Message.MessagesList(messages.subList(offset, last)), gson)
    }

    private fun getMessage(exchange: HttpServerExchange) {
        exchange.requestReceiver.receiveFullString { _, message ->
            val (_, msg) = gson.fromJson(message, Message.SimpleMessage::class.java)
            if (messages.size > MAX_MESSAGES) {
                val eldest = messages.minBy { it.id ?: error("Message must have id") }
                messages.remove(eldest)
            }
            messages.add(
                Message.SimpleMessage(
                    ((messages.maxBy { it.id ?: 0 }?.id ?: 0) + 1) % Int.MAX_VALUE,
                    msg,
                    LocalDateTime.now(),
                    users.find { it.token == exchange.token }?.id ?: -1
                )
            )
        }
        println(messages.toString())
    }

    private fun isAuthorized(exchange: HttpServerExchange): Boolean {
        val strToken = try {
            exchange.requestHeaders["Authorization"].last.split(" ").lastOrNull()
        } catch (e: IllegalStateException) {
            return false
        }
        if (strToken == null) {
            return false
        } else {
            val token = try {
                UUID.fromString(strToken)
            } catch (e: IllegalArgumentException) {
                return false
            }
            val user = users.find { it.token == token }
            return if(user == null) {
                false
            } else {
                user.lastActive = LocalDateTime.now()
                true
            }
        }
    }

    private fun error(exchange: HttpServerExchange) {
        exchange.error(400, "Bad Request.", gson)
    }

    init {
        Undertow.builder()
            .addHttpListener(port, hostName)
            .setHandler(
                Handlers.path().apply {
                    addExactPath("/login", ::login)
                    addExactPath("/logout", ::logout)
                    addPrefixPath("/users", ::users)
                    addPrefixPath("/messages", ::messages)
                    addPrefixPath("/", ::error)
                }
            )
            .build()
            .start()
        IO(this).start()
        while(work) {
            Thread.sleep(3000)
            synchronized(users) {
                users.removeIf { user ->
                    println("check user")
                    val time = user.lastActive.plusMinutes(10)
                    (time < LocalDateTime.now()).also {
                        if(it) println("REMOVE $user")
                    }
                }
            }

        }
    }

    private companion object {
        val HttpServerExchange.token: UUID
            get() = UUID.fromString(requestHeaders["Authorization"].last.split(" ").last())

        val HttpServerExchange.isPost: Boolean
            get() = requestMethod == HttpString("POST")

        val HttpServerExchange.isGet: Boolean
            get() = requestMethod == HttpString("GET")


        const val MAX_MESSAGES = 100

    }
}