package ru.nsu.leidenn

import com.google.gson.Gson
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

sealed class Message {
    data class InitMessage(val username: String?) : Message()

    data class AboutUser(
        val id: Int,
        val username: String?,
        val online: Boolean?,
        val token: String? = null
    ) : Message()

    data class SimpleMessage(
        val id: Int? = null,
        val message: String,
        val time: LocalDateTime? = null,
        val author: Int? = null
    ) : Message()

    data class UsersList(val users: List<AboutUser>) : Message()
    data class MessagesList(val messages: List<Message>) : Message()


    companion object {
        const val NOT_AUTH = "You are not authorized."
    }
}

fun HttpServerExchange.send(message: Message, gson: Gson = Gson()) {
    responseHeaders.clear()
    responseHeaders.put(Headers.CONTENT_TYPE, "application/json")
    responseSender.send(gson.toJson(message))
}

fun HttpServerExchange.error(code: Int, str: String, gson: Gson = Gson()) {
    statusCode = code
    send(
        Message.SimpleMessage(
            null,
            str,
            null
        ), gson
    )
}